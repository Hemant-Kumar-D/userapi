package com.example.allapi_call.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.example.allapi_call.R
import com.example.allapi_call.databinding.ActivityDashbordBinding

class Dashbord : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding:ActivityDashbordBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_dashbord)
      binding.userApi.setOnClickListener(this)
        binding.imageApi.setOnClickListener(this)
        binding.commentApi.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
       when(view?.id){
           R.id.user_api ->{
               startActivity(Intent(this,MainActivity::class.java))

           }
           R.id.image_api ->{
               startActivity(Intent(this,Image_Api::class.java))

           }
           R.id.comment_api ->{
               startActivity(Intent(this,CoomentApi::class.java))

           }

       }
    }
}