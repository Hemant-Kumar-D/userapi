package com.example.allapi_call.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.allapi_call.Adapter.ImageAdapter
import com.example.allapi_call.ApiRepogetry.CommentsRepogetry

import com.example.allapi_call.ApiRepogetry.Imagerepogetry
import com.example.allapi_call.ApiRepogetry.UserRepogetry
import com.example.allapi_call.Factroy.UserFactroy
import com.example.allapi_call.R
import com.example.allapi_call.Viewmodel.Userviewmodel
import com.example.allapi_call.databinding.ActivityImageApiBinding

class Image_Api : AppCompatActivity() {
    private lateinit var binding:ActivityImageApiBinding
    lateinit var factroy: UserFactroy
    lateinit var viewModel: Userviewmodel
    lateinit var adapter:ImageAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding=DataBindingUtil.setContentView(this,R.layout.activity_image_api)
        factroy= UserFactroy(UserRepogetry(), Imagerepogetry(), CommentsRepogetry())
        viewModel= ViewModelProvider(this,factroy)[Userviewmodel::class.java]

        binding.lifecycleOwner=this
        binding.imageRecycilerview.layoutManager= LinearLayoutManager(this)
        viewModel.getallimage().observe(this, Observer {
             adapter=ImageAdapter(it,this)
            binding.imageRecycilerview.adapter=adapter
            adapter.notifyDataSetChanged()
        })
    }
}