package com.example.allapi_call.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.allapi_call.Adapter.UserAdpter
import com.example.allapi_call.ApiRepogetry.CommentsRepogetry
import com.example.allapi_call.ApiRepogetry.Imagerepogetry
import com.example.allapi_call.ApiRepogetry.UserRepogetry
import com.example.allapi_call.Factroy.UserFactroy
import com.example.allapi_call.R
import com.example.allapi_call.Viewmodel.Userviewmodel
import com.example.allapi_call.databinding.ActivityMainBinding
import com.example.allapi_call.responce.AllCommentData

class MainActivity : AppCompatActivity() {
    private lateinit var binding:ActivityMainBinding
    lateinit var factroy:UserFactroy
    lateinit var viewModel: Userviewmodel

    lateinit var adapter:UserAdpter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this, R.layout.activity_main)
        factroy= UserFactroy(UserRepogetry(), Imagerepogetry(), CommentsRepogetry())
        viewModel=ViewModelProvider(this,factroy)[Userviewmodel::class.java]
        binding.lifecycleOwner=this
        binding.userRecycilerview.layoutManager=LinearLayoutManager(this)
        viewModel.getuserdata().observe(this, Observer {
            adapter=UserAdpter(it)
            binding.userRecycilerview.adapter=adapter
            adapter.notifyDataSetChanged()
        })

    }
}