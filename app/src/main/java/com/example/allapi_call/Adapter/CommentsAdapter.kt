package com.example.allapi_call.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.allapi_call.R
import com.example.allapi_call.databinding.CommentItemBinding
import com.example.allapi_call.databinding.ImageItemBinding
import com.example.allapi_call.responce.Commentdata

class CommentsAdapter(private val comment:List<Commentdata>) : RecyclerView.Adapter<CommentsAdapter.MyViewHolder>() {

    inner class MyViewHolder(var binding: CommentItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var view: View? =
            LayoutInflater.from(parent.context).inflate(R.layout.comment_item, parent, false)
        var binding: CommentItemBinding = DataBindingUtil.bind(view!!)!!
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
       return  comment.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val album = comment[position]

        holder.binding.apply {
            tvPostid.text = album.postId.toString()
            tvId.text = album.id.toString()
            tvName.text = album.name
            tvEmail.text = album.email
            tvBody.text = album.body


        }
    }
}