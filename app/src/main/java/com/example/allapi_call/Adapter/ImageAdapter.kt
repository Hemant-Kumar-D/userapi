package com.example.allapi_call.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.allapi_call.R
import com.example.allapi_call.databinding.ImageItemBinding
import com.example.allapi_call.responce.Imagedata
import com.example.allapi_call.responce.UserData
import com.squareup.picasso.Picasso

class ImageAdapter(private val user: List<Imagedata>,private val context:Context): RecyclerView.Adapter<ImageAdapter.MyViewHolder>() {

    inner class MyViewHolder(var binding: ImageItemBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var view: View? =
            LayoutInflater.from(parent.context).inflate(R.layout.image_item, parent, false)
        var binding: ImageItemBinding = DataBindingUtil.bind(view!!)!!
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val album = user[position]

        holder.binding.apply {
            tvAlbumid.text=album.albumId.toString()
            tvId.text=album.id.toString()
            tvTital.text=album.title
            Picasso.with(context).load(album.url).into(imageView)
            Picasso.with(context).load(album.thumbnailUrl).into(imageThumnel)


        }
    }





    override fun getItemCount(): Int {
        return user.size
    }


}
