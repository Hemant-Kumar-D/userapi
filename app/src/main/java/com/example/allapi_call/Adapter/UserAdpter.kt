package com.example.allapi_call.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.allapi_call.R
import com.example.allapi_call.responce.UserData
import com.example.allapi_call.databinding.UserlayoutBinding

class UserAdpter(private val user:List<UserData>): RecyclerView.Adapter<UserAdpter.MyViewHolder>() {

    inner class MyViewHolder(var binding:UserlayoutBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var view: View? = LayoutInflater.from(parent.context).inflate(R.layout.userlayout, parent, false)
        var binding: UserlayoutBinding = DataBindingUtil.bind(view!!)!!
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return user.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val album = user[position]

        holder.binding.apply {
            tvId.text=album.id.toString()
            tvUserid.text = album.userid.toString()
            tvTital.text = album.title
            tvBody.text=album.body

        }
    }

}