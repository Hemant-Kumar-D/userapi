package com.example.allapi_call.AllApi


import com.example.allapi_call.User
import com.example.allapi_call.responce.AllCommentData
import com.example.allapi_call.responce.Image
import com.example.allapi_call.responce.Imagedata
import retrofit2.Call
import retrofit2.http.GET


interface User {
    @GET("/posts")
    fun getalldata():Call<User>
    @GET("/photos")
    fun getallimagedata():Call<Image>
    @GET("/comments")
    fun  getallcomment():Call<AllCommentData>
}