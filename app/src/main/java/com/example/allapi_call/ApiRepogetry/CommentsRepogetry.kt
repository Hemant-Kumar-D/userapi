package com.example.allapi_call.ApiRepogetry

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.allapi_call.AllApi.UserRetrofitclint
import com.example.allapi_call.responce.AllCommentData
import com.example.allapi_call.responce.Commentdata
import com.example.allapi_call.responce.Image
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CommentsRepogetry{
    var userdat: MutableLiveData<AllCommentData> = MutableLiveData()
    val TAG="Checkdata"
    fun getallcomment(): LiveData<AllCommentData> {
        val call= UserRetrofitclint.user.getallcomment()
        call.enqueue(object:Callback<AllCommentData>{
            override fun onResponse(call: Call<AllCommentData>, response: Response<AllCommentData>) {
                if(response.isSuccessful){
                    var Responcedata= response.body()
                    userdat.postValue(Responcedata!!)
                    //  Log.d(TAG, "onResponse:$Responcedata ")
                }
            }

            override fun onFailure(call: Call<AllCommentData>, t: Throwable) {
                Log.d(TAG, "onFailure:${t.message}")
            }

        })
        return userdat!!
    }
}