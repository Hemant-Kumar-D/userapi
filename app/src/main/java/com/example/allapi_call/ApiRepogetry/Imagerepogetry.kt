package com.example.allapi_call.ApiRepogetry

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.allapi_call.AllApi.UserRetrofitclint

import com.example.allapi_call.responce.Image

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Imagerepogetry {
    var userdat: MutableLiveData<Image> = MutableLiveData()
    val TAG="Checkdata"
    fun getallImage(): LiveData<Image> {
        val call= UserRetrofitclint.user.getallimagedata()
        call.enqueue(object:Callback<Image>{
            override fun onResponse(call: Call<Image>, response: Response<Image>) {
                if(response.isSuccessful){
                    var Responcedata= response.body()
                    userdat.postValue(Responcedata!!)
                    //  Log.d(TAG, "onResponse:$Responcedata ")
                }
            }

            override fun onFailure(call: Call<Image>, t: Throwable) {
                Log.d(TAG, "onFailure:${t.message}")
            }

        })
        return userdat!!
    }
}