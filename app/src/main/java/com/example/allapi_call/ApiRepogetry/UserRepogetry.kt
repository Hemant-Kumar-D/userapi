package com.example.allapi_call.ApiRepogetry

import android.util.Log
import androidx.lifecycle.LiveData

import androidx.lifecycle.MutableLiveData

import com.example.allapi_call.AllApi.UserRetrofitclint
import com.example.allapi_call.User

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserRepogetry{
    var userdat:MutableLiveData<User> = MutableLiveData()
    val TAG="Checkdata"
    fun getalldata(): LiveData<User>{
        val call=UserRetrofitclint.user.getalldata()
        call.enqueue(object:Callback<User>{
            override fun onResponse(call: Call<User>, response: Response<User>) {
               if(response.isSuccessful){
                  var Responcedata= response.body()
                   userdat.postValue(Responcedata!!)
                 //  Log.d(TAG, "onResponse:$Responcedata ")
               }
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
                Log.d(TAG, "onFailure:${t.message}")
            }

        })
        return userdat!!
    }
}