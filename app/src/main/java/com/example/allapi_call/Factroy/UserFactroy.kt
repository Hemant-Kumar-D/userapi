package com.example.allapi_call.Factroy

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.allapi_call.ApiRepogetry.CommentsRepogetry
import com.example.allapi_call.ApiRepogetry.Imagerepogetry
import com.example.allapi_call.ApiRepogetry.UserRepogetry
import com.example.allapi_call.Viewmodel.Userviewmodel

class UserFactroy(private val userrepo: UserRepogetry,private  val imagerepo: Imagerepogetry,private val allcoment: CommentsRepogetry):ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(Userviewmodel::class.java)) {
            return Userviewmodel(userrepo,imagerepo,allcoment) as T
        }
        throw IllegalArgumentException("Unknown class")
    }
}